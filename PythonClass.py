#!/usr/bin/env python3

__author__ = "Mateusz Garbiak"

class GameConsoles: pass
class Xbox(GameConsoles): pass
class PlayStation(GameConsoles): pass
class XboxOne(Xbox): pass
class Xbox360(Xbox): pass
class PlayStation4(PlayStation): pass
class PlayStation3(PlayStation): pass

xbox_one_standard = XboxOne()
xbox_one_s = XboxOne()
xbox_one_x = XboxOne()

xbox_360_core = Xbox360()
xbox_360_arcade = Xbox360()
xbox_360_premium = Xbox360()
xbox_360_elite = Xbox360()
xbox_360_s = Xbox360()
xbox_360_e = Xbox360()

play_station_4_standard = PlayStation4()
play_station_4_slim = PlayStation4()
play_station_4_pro = PlayStation4()

play_station_3_fat = PlayStation3()
play_station_3_slim = PlayStation3()
play_station_3_super_slim = PlayStation3()

print (xbox_one_standard)
print (xbox_360_arcade)
print (play_station_4_pro)
print (play_station_3_fat)
